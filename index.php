<?php
if (isset($_GET['_escaped_fragment_'])):
    if ($_GET['_escaped_fragment_'] == 'logo'):               include_once("snapshots/_logo.php");           endif;
    if ($_GET['_escaped_fragment_'] == 'books'):              include_once("snapshots/_books.php");          endif;
    if ($_GET['_escaped_fragment_'] == 'business_cards'):     include_once("snapshots/_business_cards.php"); endif;
    if ($_GET['_escaped_fragment_'] == 'illustration'):       include_once("snapshots/_illustration.php");   endif; 
    if ($_GET['_escaped_fragment_'] == 'posters'):            include_once("snapshots/_posters.php");        endif; 
    if ($_GET['_escaped_fragment_'] == 'pop_art'):            include_once("snapshots/_pop_art.php");        endif;
    if ($_GET['_escaped_fragment_'] == 'ilva_kalnberza'):     include_once("snapshots/_ilva_kalnberza.php"); endif;
else:
    include_once("main.php"); 
endif;
?>