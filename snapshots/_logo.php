<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="fragment" content="!" />
<title>Ilva Kalnberza | Graphic Design Portfolio</title>
<link rel="favicon.ico" href="favicon.ico" />
<link rel="stylesheet" href="contactable/contactable.css" type="text/css" />
<link rel="stylesheet" href="css/galleriffic-2.css" type="text/css" />

<style type="text/css">

html, body {
	margin:0;
	/* [disabled]padding:0; */
}

#main, #top_menu {
   top: 0px;
   right: 0px;
   bottom: 0px;
   left: 0px;
   width:1020px;
   height: 100%;
   margin: auto;} 
 
a img {border:none;}

ul {padding-left:0;
    margin-left:0;}
   
li {background-color:black;
    list-style: none;
	margin-bottom:10px;
	padding:2px;

}

a { font-size:12px;
    font-family:Verdana;
    text-decoration:none;
    color:white;
	font-weight:bold;}
	
.hover { color:red;}

.red { color:red;}

a.top { color:black;}

a.top:hover { color: red;}

a.left:hover { color: red;}

</style>

<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery.galleriffic.js"></script>
<script type="text/javascript" src="js/jquery.opacityrollover.js"></script>
<script type="text/javascript" src="contactable/jquery.validate.pack.js"></script>
<script type="text/javascript" src="contactable/jquery.contactable.js"></script>
</head>

<body>

<!-- Facebook -->
<div id="fb-root"></div>

<!-- Top Right menu -->
<div id="top_menu" style="position:relative; height:100px; background-color:#fff; width:1024px;left:2px;">
  <div id="top_links" style="position:relative; top:75px; text-align:right; padding-right:47px;"> 
    
       <a  href="/" id="t1" class="top" style="margin-left:40px;">WORK</a>
       <a  href="#!ilva_kalnberza" onclick="javascript:loadContent('#main_board', 'about.php');topsel('#t2');" class="top" id="t2" style="margin-left:40px;">ABOUT</a>
         
  </div>
</div>

<!-- Content -->
<div  id="main" style="z-index:1; position:relative;"> <img src="/img/background.jpg" style="position:absolute;" alt="">
  <!-- Main Menu -->
  <div id="menu" style="position:absolute; width:200px;height:507px; top:0px; left:50px; background-color:#fff;z-index:2;">
    <div style="padding-left:20px;"> <a href="/"><img src="/img/logo.jpg" /></a>
      <ul>
        <li> <a id="l4" class="left" onclick="javascript:loadContent('#main_board', '_illustration.php');leftsel('#l4');" href="#!illustration">ILLUSTRATION</a> </li>
        <li> <a id="l2" class="left" onclick="javascript:loadContent('#main_board', '_works.php');leftsel('#l2');" href="#!posters">POSTERS</a> </li>
        <li> <a id="l5" class="left" onclick="javascript:loadContent('#main_board', '_popart.php');leftsel('#l5');" href="#!pop_art">POP ART</a> </li>
        <li> <a id="l1" class="left" onclick="javascript:loadContent('#main_board', '_books.php');leftsel('#l1');" href="#!books">BOOKS</a> </li>
        <li> <a id="l3" class="left" onclick="javascript:loadContent('#main_board', '_cards.php');leftsel('#l3');" href="#!business_cards">CARDS</a> </li>
        <li> <a id="l6" class="left" onclick="javascript:loadContent('#main_board', '_logo.php');leftsel('#l6');" href="#!logo">LOGO</a> </li>
      </ul>
    </div>
  </div>
  
<!-- Main Area -->
<div id="main_board" style="width:770px; height:507px; z-index:1000; position:relative;left:250px;font-family:Verdana;font-size:12px;font-weight:bold;background-image:url(img/background-sub.jpg);">
    <div id="gallery" class="content">
      <div id="caption" class="caption-container"></div>
      <div class="slideshow-container">
        <div id="loading" class="loader"></div>
        <div id="slideshow" class="slideshow"></div>
      </div>
    </div>
    <div id="thumbs" class="navigation">
    <ul class="thumbs">
      <li> <a class="thumb" name="drop" href="/img/logo/1.jpg" title="click"> <img src="/img/logo/_th_1.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR AN ARCHITECTURAL PRACTICE SPECIALIZING IN ECO FRIENDLY MATERIALS AND NATURE INSPIRED FORMS </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/2.jpg" title="click"> <img src="/img/logo/_th_2.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR A LAW FIRM WHICH HAD TO REPRESENT  STABILITY AND PROGRESS </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/3.jpg" title="click"> <img src="/img/logo/_th_3.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR AN AUTO TUNING COMPANY WHICH SPECIALIZING  IN AUTO COSMETIC SERVICES </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/4.jpg" title="click"> <img src="/img/logo/_th_4.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR A COMPANY IN A TOURISTIC INDUSTRY. IT’S AIM IS TO OFFER  SHORT STAY ACCOMODATION IN FOREST SURROUNDINGS WITH AN ACCESS TO THE SEASIDE </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/5.jpg" title="click"> <img src="/img/logo/_th_5.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR A NON PROFIT BIRDWATCHING SOCIETY </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/l6.jpg" title="click"> <img src="/img/logo/_th_l6.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR THE FOUNDER OF NANO COURIERS </div>
      </li>
    </ul>
    </div>
</div>
  
<!-- Close Content -->
</div>

<!-- Contacts-->
<div id="contactable"></div>

</body>

<script type="text/javascript" src="js/page-service.js"></script>
<script type="text/jscript">

/**
 * On Document Ready
 *
 */
jQuery(document).ready(function($) {
	
	
/**
 * Onload
 *
 */
 function onload() {
    var hash = window.location.hash.substring(1);
	
	     if (hash == "!ilva_kalnberza") {loadContent('#main_board', 'about.php');topsel('#t2');}
	else if (hash == "!business_cards") {loadContent('#main_board', '_cards.php');leftsel('#l3');}
	else if (hash == "!illustration") {loadContent('#main_board', '_illustration.php');leftsel('#l4');}
	else if (hash == "!posters") {loadContent('#main_board', '_works.php');leftsel('#l2');}
	else if (hash == "!pop_art") {loadContent('#main_board', '_popart.php');leftsel('#l5');}
	else if (hash == "!books") {loadContent('#main_board', '_books.php');leftsel('#l1');}
	else if (hash == "!logo") {loadContent('#main_board', '_logo.php');leftsel('#l6');}
	else if (hash == "") {topsel('#t1');}
 }	
	

/**
 *Contacts
 *
 */
 $(function(){$('#contactable').contactable({subject: 'feedback URL:'+location.href});});


/**
 * First screen animation
 *
 */
 $("#musa_image, #l1").hover( 
    function(){$("#musa_image").animate({width: 150, top: 68, left: 98, height:150}, 300);$('#l1').addClass("hover");},         
    function(){$("#musa_image").animate({width: 136, top: 75, left: 105, height:136}, 300);$('#l1').removeClass("hover");} 
 ); 

 $("#web_image, #l3").hover( 
    function(){$("#web_image").animate({width: 150, top: 68, left: 312, height:150}, 300);$('#l3').addClass("hover");},         
    function(){$("#web_image").animate({width: 136, top: 75, left: 320, height:136}, 300);$('#l3').removeClass("hover");} 
 );

 $("#lips_image, #l4").hover( 
    function(){$("#lips_image").animate({width: 150, top: 68, left: 527, height:150}, 300);$('#l4').addClass("hover");},         
    function(){$("#lips_image").animate({width: 136, top: 75, left: 535, height:136}, 300);$('#l4').removeClass("hover");} 
 );

 $("#birdy_image, #l2").hover( 
    function(){$("#birdy_image").animate({width: 150, top: 268, left: 208, height:150}, 300);$('#l2').addClass("hover");},         
    function(){$("#birdy_image").animate({width: 136, top: 275, left: 215, height:136}, 300);$('#l2').removeClass("hover");} 
 );

 $("#pop_image, #l5").hover( 
    function(){$("#pop_image").animate({width: 150, top: 268, left: 422, height:150}, 300);$('#l5').addClass("hover");},         
    function(){$("#pop_image").animate({width: 136, top: 275, left: 430, height:136}, 300);$('#l5').removeClass("hover");} 
 );


/**
 * End On Document Ready
 *
 */
});



/**
 * Load Content
 *
 */
function loadContent(elementSelector, sourceUrl) {
$("#main_board").load(sourceUrl);
}


/**
 * Mark selected side-menu
 *
 */
function leftsel(a) {
	for (var i=1; i<7; i++)
	 { $('#l' + i).removeClass("red"); }
	
    $(a).addClass("red");
	
	for (var i=1; i<3; i++)
	 { $('#t' + i).removeClass("red"); 
	   $('#t' + i).addClass("top");
	 }

     $('#t1').removeClass("top");
     $('#t1').addClass("red"); 	 
}


/**
 * Mark selected top-menu
 *
 */
function topsel(a) {
	for (var i=1; i<3; i++)
	 { $('#t' + i).removeClass("red"); 
	   $('#t' + i).addClass("top");
	 }
	   
	for (var i=1; i<7; i++)
	 { $('#l' + i).removeClass("red"); }
	
       $(a).removeClass("top");
       $(a).addClass("red"); 
}


/**
 * Google
 *
 */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-37373307-1']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();


/**
 * IE6 Fix
 *
 */
<?php if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false): ?>
$("a.left").hover(  
   function () { 
      $(this).addClass("hover");
	           }, 
   function () {    
	  	     $(this).removeClass("hover"); 
               }); 
<?php  endif; ?>


/**
 * Facebook
 *
 */
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


</script>
</html>