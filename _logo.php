<meta http-equiv="Content-type" content="text/html; charset=utf-8">

<div id="main_board" style="background-image:url(img/background-sub.jpg);width:770px; height:505px; position:relative;font-family:Verdana;
font-size:12px; font-weight:bold; ">
  <div id="gallery" class="content">
    <div id="caption" class="caption-container"></div>
    <div class="slideshow-container">
      <div id="loading" class="loader"></div>
      <div id="slideshow" class="slideshow"></div>
    </div>
  </div>
  <div id="thumbs" class="navigation">
    <ul class="thumbs">
      <li> <a class="thumb" name="drop" href="/img/logo/1.jpg" title="click"> <img src="/img/logo/_th_1.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR AN ARCHITECTURAL PRACTICE SPECIALIZING IN ECO FRIENDLY MATERIALS AND NATURE INSPIRED FORMS </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/2.jpg" title="click"> <img src="/img/logo/_th_2.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR A LAW FIRM WHICH HAD TO REPRESENT  STABILITY AND PROGRESS </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/3.jpg" title="click"> <img src="/img/logo/_th_3.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR AN AUTO TUNING COMPANY WHICH SPECIALIZING  IN AUTO COSMETIC SERVICES </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/4.jpg" title="click"> <img src="/img/logo/_th_4.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR A COMPANY IN A TOURISTIC INDUSTRY. IT’S AIM IS TO OFFER  SHORT STAY ACCOMODATION IN FOREST SURROUNDINGS WITH AN ACCESS TO THE SEASIDE </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/5.jpg" title="click"> <img src="/img/logo/_th_5.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR A NON PROFIT BIRDWATCHING SOCIETY </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/logo/l6.jpg" title="click"> <img src="/img/logo/_th_l6.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> LOGO FOR THE FOUNDER OF NANO COURIERS </div>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript" src="js/page-service.js"></script>