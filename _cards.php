<meta http-equiv="Content-type" content="text/html; charset=utf-8">

<div id="main_board" style="background-image:url(img/background-sub.jpg);width:770px; height:505px; position:relative;font-family:Verdana;
font-size:12px; font-weight:bold; ">
  <div id="gallery" class="content">
    <div id="caption" class="caption-container"></div>
    <div class="slideshow-container">
      <div id="loading" class="loader"></div>
      <div id="slideshow" class="slideshow"></div>
    </div>
  </div>
  <div id="thumbs" class="navigation">
    <ul class="thumbs">
      <li> <a class="thumb" name="drop" href="/img/cards/1c.jpg" title="click"> <img src="/img/cards/_th_1c.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BUSINESS CARD FOR TAXI DRIVER </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/cards/2c.jpg" title="click"> <img src="/img/cards/_th_2c.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BUSINESS CARD FOR THE FOUNDER OF NANO COURIERS </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/cards/1.jpg" title="click"> <img src="/img/cards/_th_1.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> SELFPROMOTIONAL BUSINESS CARD </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/cards/2.jpg" title="click"> <img src="/img/cards/_th_2.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BUSINESS CARD FOR AN AUTO TUNING COMPANY WHICH SPECIALIZING IN AUTO COSMETIC SERVICES </div>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript" src="js/page-service.js"></script>