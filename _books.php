<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<div id="main_board" style="background-image:url(img/background-sub.jpg);width:770px; height:505px; position:relative;font-family:Verdana; font-size:12px; font-weight:bold; ">
  <div id="gallery" class="content">
    <div id="caption" class="caption-container"></div>
    <div class="slideshow-container">
      <div id="loading" class="loader"></div>
      <div id="slideshow" class="slideshow"></div>
    </div>
  </div>
  <div id="thumbs" class="navigation">
    <ul class="thumbs">
      <li> <a href="/img/books/1.jpg" name="drop" title="click" class="thumb" id="drop"> <img src="/img/books/_th_1.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOOK COVER &amp; ILLUSTRATION.
          PUBLISHED 2012 BY APOSTROFS, LATVIA. </div>
      </li>
      <li> <a href="/img/books/2.jpg" name="drop" title="click" class="thumb" id="drop"> <img src="/img/books/_th_2.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOOK COVER &amp; ILLUSTRATION.
          PUBLISHED 2010 BY MANSARDS, LATVIA. </div>
      </li>
      <li> <a href="/img/books/3b.jpg" name="drop" title="click" class="thumb" id="drop"> <img src="/img/books/_th_3b.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOOK COVER &amp; LAYOUT<br />
          PUBLISHED 2014 BY BLURB </div>
      </li>
      <li> <a href="/img/books/5b.jpg" name="drop" title="click" class="thumb" id="drop"> <img src="/img/books/_th_5b.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOOK COVER, ILLUSTRATION, LAYOUT
          PUBLISHED 2014 BY BLURB </div>
      </li>
      <li> <a href="/img/books/3.jpg" name="drop" title="click" class="thumb" id="drop"> <img src="/img/books/_th_3.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOOK COVER &amp; ILLUSTRATION.
          PUBLISHED 2011 BY MANSARDS, LATVIA. </div>
      </li>
      <li> <a href="/img/books/4.jpg" name="drop" title="click" class="thumb" id="drop"> <img src="/img/books/_th_4.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOOK COVER &amp; ILLUSTRATION.
          BOOK ILLUSTRATION COMPETITION 
          BY FOLIO SOCIETY, 2012. </div>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript" src="js/page-service.js"></script>