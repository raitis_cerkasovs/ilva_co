<meta http-equiv="Content-type" content="text/html; charset=utf-8">

<div id="main_board" style="background-image:url(img/background-sub.jpg);width:770px; height:505px; position:relative;font-family:Verdana;
font-size:12px; font-weight:bold; ">
  <div id="gallery" class="content">
    <div id="caption" class="caption-container"></div>
    <div class="slideshow-container">
      <div id="loading" class="loader"></div>
      <div id="slideshow" class="slideshow"></div>
    </div>
  </div>
  <div id="thumbs" class="navigation">
    <ul class="thumbs">
      <li> <a class="thumb" name="drop" href="/img/illustration/1i.jpg" title="click"> <img src="/img/illustration/_th_1i.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> TEA ADVENTURES, 2014 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/illustration/2i.jpg" title="click"> <img src="/img/illustration/_th_2i.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> PARIS, 2014 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/illustration/3i.jpg" title="click"> <img src="/img/illustration/_th_3i.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> ICE-CREAM HAPPINESS, 2014 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/illustration/4i.jpg" title="click"> <img src="/img/illustration/_th_4i.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> TO BE DIFFERENT, 2014 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/illustration/01.jpg" title="click"> <img src="/img/illustration/_th_01.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> THAT’S WHERE THE FOOD IS DISAPPEARING AT NIGHT, 2013 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/illustration/04.jpg" title="click"> <img src="/img/illustration/_th_04.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> ILLUSTRATION FOR THE BOOK COMPETITION BY FOLIO SOCIETY. BRAVE NEW WORLD BY ALDOUS HUXLEY, 2013 </div>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript" src="js/page-service.js"></script>