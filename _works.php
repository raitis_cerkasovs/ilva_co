<meta http-equiv="Content-type" content="text/html; charset=utf-8">

<div id="main_board" style="background-image:url(img/background-sub.jpg);width:770px; height:505px; position:relative;font-family:Verdana;
font-size:12px; font-weight:bold; ">
  <div id="gallery" class="content">
    <div id="caption" class="caption-container"></div>
    <div class="slideshow-container">
      <div id="loading" class="loader"></div>
      <div id="slideshow" class="slideshow"></div>
    </div>
  </div>
  <div id="thumbs" class="navigation">
    <ul class="thumbs">
      <li> <a class="thumb" name="drop" href="/img/posters/6.jpg" title="click"> <img src="/img/posters/_th_6.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> POSTER - STOP WASTING FOOD - FOR NORDIC-BALTIC AD COMPETITION, 2013. </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/posters/5.jpg" title="click"> <img src="/img/posters/_th_5.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> POSTER DESIGN
          FOR AN ART EXHIBITION, 2012, LONDON. </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/posters/1.jpg" title="click"> <img src="/img/posters/_th_1.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> POSTER & FLYER DESIGN
          FOR BRAZUKA (DJS) PARTY & AN EXHIBITION
          2010, LONDON. </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/posters/2.jpg" title="click"> <img src="/img/posters/_th_2.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> POSTER & FLYER DESIGN
          FOR BRAZUKA (DJS) PARTY & AN EXHIBITION
          2010, LONDON. </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/posters/3.jpg" title="click"> <img src="/img/posters/_th_3.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> POSTER & FLYER DESIGN
          FOR COMMUNITY SHARING DAY, 2011, HACKNEY. </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/posters/4.jpg" title="click"> <img src="/img/posters/_th_4.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> POSTER & FLYER DESIGN
          FOR BRAZUKA (DJS) PARTY, 2010, LONDON. </div>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript" src="js/page-service.js"></script>
