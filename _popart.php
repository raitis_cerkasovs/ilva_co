<meta http-equiv="Content-type" content="text/html; charset=utf-8">

<div id="main_board" style="background-image:url(img/background-sub.jpg);width:770px; height:505px; position:relative;font-family:Verdana;
font-size:12px; font-weight:bold; ">
  <div id="gallery" class="content">
    <div id="caption" class="caption-container"></div>
    <div class="slideshow-container">
      <div id="loading" class="loader"></div>
      <div id="slideshow" class="slideshow"></div>
    </div>
  </div>
  <div id="thumbs" class="navigation">
    <ul class="thumbs">
      <li> <a class="thumb" name="drop" href="/img/popart/6.jpg" title="click"> <img src="/img/popart/_th_6.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOTTLE CAP POP ART <br />
          <br />
          TITLE: AUDREY<br />
          MATERIALS: BOTTLE TOPS, ACRYLIC, PLASTIC, CANVAS 30 X 40 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/popart/5.jpg" title="click"> <img src="/img/popart/_th_5.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOTTLE CAP POP ART <br />
          <br />
          TITLE: A BLEEDING ROSE<br />
          MATERIALS: BOTTLE TOPS, ACRYLIC, PLASTIC, CANVAS 30 X 40 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/popart/3.jpg" title="click"> <img src="/img/popart/_th_3.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOTTLE CAP POP ART <br />
          <br />
          TITLE: RAY<br />
          MATERIALS: BOTTLE TOPS, ACRYLIC, CANVAS 30 X 40 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/popart/4.jpg" title="click"> <img src="/img/popart/_th_4.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOTTLE CAP POP ART <br />
          <br />
          TITLE: KATE & WILL<br />
          MATERIALS: BOTTLE TOPS, ACRYLIC, CANVAS 30 X 40 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/popart/1.jpg" title="click"> <img src="/img/popart/_th_1.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOTTLE CAP POP ART <br />
          <br />
          TITLE: YOU<br />
          MATERIALS: BOTTLE TOPS, ACRYLIC, CANVAS 30 X 40 </div>
      </li>
      <li> <a class="thumb" name="drop" href="/img/popart/2.jpg" title="click"> <img src="/img/popart/_th_2.jpg" alt="click" /> </a>
        <div class="caption" style="position:relative; top:380px; left:-307px; color:white; width:270px;"> BOTTLE CAP POP ART <br />
          <br />
          TITLE: JIMMI<br />
          MATERIALS: BOTTLE TOPS, ACRYLIC, CANVAS  30 X 40 </div>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript" src="js/page-service.js"></script>